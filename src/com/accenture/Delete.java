package com.accenture;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Delete {
	public static void main(String[] args) {
		// we will talk to database
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("unit1"); // load database configuration,
																						// open database
		EntityManager em = factory.createEntityManager(); // rest of the operations will be handled by entitymanager

		em.getTransaction().begin();

		Product temp = em.find(Product.class, 115);
		if (temp != null) {
			em.remove(temp); // delete from product where id = 104
		}else {
			System.out.println("could not delete product as it is not available");
		}
		
		em.getTransaction().commit();

		em.close();
		factory.close();
	}
}
