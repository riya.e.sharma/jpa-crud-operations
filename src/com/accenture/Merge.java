package com.accenture;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Merge {
	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("unit1"); // load database configuration,
		EntityManager em = factory.createEntityManager(); // rest of the operations will be handled by entitymanager

		em.getTransaction().begin();

		Product p = new Product(116, "monitor", 13000, 5);
		em.merge(p);
		
		em.getTransaction().commit();

		em.close();
		factory.close();

	}
}

 