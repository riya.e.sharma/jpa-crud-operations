package com.accenture;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Select {
	public static void main(String[] args) {
		//we will talk to database
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("unit1"); //load database configuration, open database
		EntityManager em = factory.createEntityManager();	//rest of the operations will be handled by entitymanager
		
		Product temp = em.find(Product.class, 104);		//select * from product where id = 102
		//jpql
		
		System.out.println(temp);
	
		em.close();
		factory.close();
	}
}
